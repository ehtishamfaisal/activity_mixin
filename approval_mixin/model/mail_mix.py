# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo.exceptions import UserError


class MailActivityType(models.Model):
	_inherit = "mail.activity.type"

	category = fields.Selection(selection_add=[('approval', 'Record Approval')])


	@api.multi
	def print_some_statement(self):
		print("print test data")

class MailActivity(models.Model):
	_inherit = "mail.activity"
	
	category = fields.Selection(related='activity_type_id.category', store=True)
	# Accept Button Method 
	def action_feedback(self, feedback=False):
		message = self.env['mail.message']
		if feedback:
			self.write(dict(feedback=feedback))
		for activity in self:
			record = self.env[activity.res_model].browse(activity.res_id)
			if activity.activity_type_id.category == 'approval':
				record.message_post_with_view(
					'approval_mixin.activity_approver_msg_done',
					values={'activity': activity},
					subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
					mail_activity_type_id=activity.activity_type_id.id,
				)
			else:
				record.message_post_with_view(
					'mail.message_activity_done',
					values={'activity': activity},
					subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
					mail_activity_type_id=activity.activity_type_id.id,
				)
			message |= record.message_ids[0]

		self.action_record_approve()
		self.unlink()
		return message.ids and message.ids[0] or False


	def action_record_approve(self):
		print("action_record_approve")

	# Hold Button Method
	def action_record_hold(self):
		message = self.env['mail.message']
		for activity in self:
			record = self.env[activity.res_model].browse(activity.res_id)
			if activity.activity_type_id.category == 'approval':
				record.message_post_with_view(
					'approval_mixin.activity_approver_msg_hold',
					values={'activity': activity},
					subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
					mail_activity_type_id=activity.activity_type_id.id,
				)
			message |= record.message_ids[0]
		self.unlink()
		return message.ids and message.ids[0] or False

	#Reject Button Done Method 
	def action_record_reject(self, feedback=False):
		message = self.env['mail.message']
		if feedback:
			self.write(dict(feedback=feedback))
		for activity in self:
			record = self.env[activity.res_model].browse(activity.res_id)
			if activity.activity_type_id.category == 'approval':
				record.message_post_with_view(
					'approval_mixin.activity_approver_msg_reject',
					values={'activity': activity},
					subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
					mail_activity_type_id=activity.activity_type_id.id,
				)
			else:
				record.message_post_with_view(
					'mail.message_activity_done',
					values={'activity': activity},
					subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_activities'),
					mail_activity_type_id=activity.activity_type_id.id,
				)
			message |= record.message_ids[0]

		self.unlink()
		return message.ids and message.ids[0] or False