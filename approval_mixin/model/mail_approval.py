# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import itertools

from odoo import api, fields, models, exceptions
import datetime


class AbstractMailAprovers(models.AbstractModel):
    _name = 'mail.approval'

    mail_is_approver = fields.Boolean(
        'Is Approver', compute='_compute_is_approver', search='_search_is_approver')
    mail_approver_ids = fields.One2many(
        'mail.approvers', 'res_id', string='Approvers',
        domain=lambda self: [('res_model', '=', self._name)])
    mail_user_ids = fields.Many2many(
        comodel_name='res.users', string='Approvers (Users)',
        compute='_get_approvers', search='_search_approver_users')

    mail_approver_count = fields.Integer('Approver Count', compute='_compute_approver_count')
    mail_approver_count_approved = fields.Integer('Approved Count', compute='_compute_approver_count')
    mail_approver_count_rejected = fields.Integer('Rejected Count', compute='_compute_approver_count')
    mail_approval_start_date = fields.Datetime(string='Approval Start Date', readonly=True)
    mail_approval_end_date = fields.Datetime(string='Approval End Date', readonly=True)
    mail_approval_state = fields.Selection([
        ('new', 'New'),
        ('review', 'Under Review'),
        ('hold', 'On Hold'),
        ('stop', 'Stopped'),
        ('approved', 'Approved'),
        ('rejected', 'Rejected')],
        string='Approval Status', copy=False, index=True, default='new')
    mail_approval_current_user_id = fields.One2many('res.users', compute='_get_approver_current_user_id')

    '''
    # todo Rules of approbation to be set-up on the PO
    # todo Template for rule of approbation
    # todo Gestion de l'approbation des work tickets
    # todo Gestion de l'approbation des RFI, ECN, VCR (taken care of by mail.approvers_config aka res_model and sequence)
    # todo Gestion de l'approbation to be done by Model
    # todo Approbation avec renvoi automatique è la personne suivante selon la séquence
    '''

    @api.one
    @api.depends('mail_approver_ids')
    def _get_approvers(self):
        self.mail_user_ids = self.mail_approver_ids.mapped('user_id')


    @api.model
    def _get_approver_current_user_id(self):
        for record in self:
            approvers = record.mail_approver_ids.sudo().search([
                ('state', 'in', ['new', 'hold']), ('res_id', '=', record.id)], order='sequence asc', limit=1)
            if approvers:
                if approvers.user_id:
                    res = approvers.user_id
                else:
                    res = False
                record.mail_approval_current_user_id = res
            
    @api.model
    def _search_approver_users(self, operator, operand):
        """
        Search function for mail_approver_ids
        Do not use with operator 'not in'. Use instead mail_is_approvers
        """
        # todo Make it work with not in
        assert operator != "not in", "Do not search mail_approver_ids with 'not in'"
        approvers = self.env['mail.approvers'].sudo().search([
            ('res_model', '=', self._name),
            ('user_id', operator, operand)])
        # using read() below is much faster than approvers.mapped('res_id')
        return [('id', 'in', [res['res_id'] for res in approvers.read(['res_id'])])]


    @api.multi
    @api.depends('mail_approver_ids')
    def _compute_is_approver(self):
        approvers = self.env['mail.approvers'].sudo().search([
            ('res_model', '=', self._name),
            ('res_id', 'in', self.ids),
            ('user_id', '=', self.env.user.id),
            ])
        # using read() below is much faster than approvers.mapped('res_id')
        following_ids = [res['res_id'] for res in approvers.read(['res_id'])]
        for record in self:
            record.mail_is_approver = record.id in following_ids

    @api.model
    def _search_is_approver(self, operator, operand):
        approvers = self.env['mail.approvers'].sudo().search([
            ('res_model', '=', self._name),
            ('user_id', '=', self.env.user.id),
            ])
        # Cases ('mail_is_approver', '=', True) or  ('mail_is_approver', '!=', False)
        if (operator == '=' and operand) or (operator == '!=' and not operand):
            # using read() below is much faster than approvers.mapped('res_id')
            return [('id', 'in', [res['res_id'] for res in approvers.read(['res_id'])])]
        else:
            # using read() below is much faster than approvers.mapped('res_id')
            return [('id', 'not in', [res['res_id'] for res in approvers.read(['res_id'])])]

    @api.multi
    def action_mail_approval_start(self):
        if self._context:
            mail_approvers = self.env['mail.approvers'].search([('res_id','=',self._context.get('default_res_id')),('res_model','=',self._context.get('default_model'))])
            mail_activity = self.env['mail.activity']
            res_model_id = self.env['ir.model'].search([('model','=',self._context.get('default_model'))])
            if mail_approvers:
                for approver in mail_approvers:
                    if approver.user_id:
                        if approver.mail_approval_state == 'new':
                            vals = {
                                'activity_type_id': approver.approval_type.id,
                                'date_deadline': datetime.datetime.now(),
                                'user_id': approver.user_id.id,
                                'res_id' :self._context.get('default_res_id'),
                                'res_model' :self._context.get('default_model'),
                                'res_model_id': res_model_id.id,
                                'mail_approver_id': approver.id,
                            }
                            mail_activity.create(vals)
                            approver.mail_approval_state = 'review'
                        elif approver.mail_approval_state == 'review':
                            print("Do Nothing")
                        else:
                            approver.mail_approval_state = 'review'
            else:
                raise exceptions.ValidationError('Approvers must be added before to start the approval process.')

    @api.multi
    def action_mail_approval_pause(self):
        print('approval pause')
        for record in self:
            print('approval pause')
            '''
            if record.mail_approver_ids:
                # do something
            else:
                raise exceptions.ValidationError('Approvers must be added before to start the approval process.')
            '''

    @api.multi
    def action_mail_approval_stop(self):
        print('approval stop')
        for record in self:
            print('approval stop')
            '''
            if record.mail_approver_ids:
                # do something
                print('approval stop')
            else:
                raise exceptions.ValidationError('Approvers must be added before to start the approval process.')
            '''

    @api.multi
    def _compute_approver_count(self):
        read_group_var = self.env['mail.approvers'].read_group([('res_id', 'in', self.ids), ('res_model', '=', self._name)],
                                                               fields=['res_id'],
                                                               groupby=['res_id'])
        approver_count_dict = dict((d['res_id'], d['res_id_count']) for d in read_group_var)
        for record in self:
            record.mail_approver_count = approver_count_dict.get(record.id, 0)
            record.mail_approver_count_approved =  record.mail_approver_ids.sudo().search_count([
                                                    ('state', '=', 'approved'),
                                                    ('res_id', '=', record.id)
                                                    ])
            record.mail_approver_count_rejected = record.mail_approver_ids.sudo().search_count([
                                                    ('state', '=', 'rejected'),
                                                    ('res_id', '=', record.id)
                                                    ])

    @api.multi
    def action_mail_edit_approver(self):
        res_model = self._name
        res_id = self.id
        return self.env['mail.approvers'].act_window_display_approver(res_model=res_model, res_id=res_id)


    '''
    @api.multi
    def prepare_vals(self, type):
        self.ensure_one
        if not self.state == 'submitted':
            raise ValidationError("The document must be in the submitted state to activate the validation process.")
        else:
            context = self._context
            current_uid = context.get('uid')
            vals = {
                'res_users': current_uid,
                'res_id': self.id,
                'res_model': self._name,
                'approval_type': type,
            }
            return vals
    '''

    @api.multi
    def mail_approvers_approve(self):
        vals = self.prepare_vals(type='rate')
        return self.action_doc_log_approval(vals)

    @api.multi
    def mail_approvers_reject(self):
        vals = self.prepare_vals(type='quantity')
        return self.action_doc_log_approval(vals)

    @api.multi
    def action_doc_log_validation_overall(self):
        vals = self.prepare_vals(type='overall')
        return self.action_doc_log_approval(vals)

    @api.multi
    def action_doc_log_approval(self, vals):
        # Verify if the doc is already approved by the user
        search_domain = []
        for key in vals.keys():
            search_domain.append((key, '=', vals[key]))
        exist_rec = self.env['doc.approval.line'].search(search_domain)

        if not exist_rec:
            new_rec = self.env['doc.approval.line'].create(vals)
            message = ('The document was correctly validated for %s.' % new_rec.approval_type)
            self.message_post(body=message, subject="Document Validation", subtype="mt_note")
        else:
            print('user already approved the document')

    def _track_subtype(self, init_values):
        if 'approval_ids' in init_values:
            return 'mail.mt_comment'
        return False

    def custom_search_count(self, res_id):
        approved_count = self.env['mail.approvers'].sudo().search_count([
                                                    ('state', '=', 'approved'),
                                                    ('res_id', '=', res_id)
                                                    ])

        rejected_count = self.env['mail.approvers'].sudo().search_count([
                                                    ('state', '=', 'rejected'),
                                                    ('res_id', '=', res_id)
                                                    ])
        total_count = self.env['mail.approvers'].sudo().search_count([
                                                    ('res_id', '=', res_id)
                                                    ])
        vals =  {
                    'approved_count':approved_count,
                    'rejected_count':rejected_count,
                    'total_count':total_count,
                    }
        return vals