odoo.define('approval_mixin.mail_chatter', function (require) {
"use strict";

var Activity = require('mail.Activity');
var AttachmentBox = require('mail.AttachmentBox');
var ChatterComposer = require('mail.composer.Chatter');
var Dialog = require('web.Dialog');
var Followers = require('mail.Followers');
var ThreadField = require('mail.ThreadField');
var mailUtils = require('mail.utils');

var concurrency = require('web.concurrency');
var config = require('web.config');
var core = require('web.core');
var Widget = require('web.Widget');

var _t = core._t;
var QWeb = core.qweb;

var MailChatter = require('mail.Chatter');

MailChatter.include({
    init: function () {
        this._super.apply(this, arguments);
        this.events = _.extend(this.events, {
            'click .o_play_btn': '_onPlayButton',
            'click .o_pause_btn': '_onPauseButton',
            'click .o_stop_btn': '_onStopButton',
            'click .o_approvers_btn': '_onApproversButton',
        });
    },

    start: function () {
        var self = this;
        // var count = 0;
        var res_id = this.record.data.id;
        self._rpc({
                model: 'mail.approval',
                method: 'custom_search_count',
                args: [[]],
                kwargs: {res_id: res_id},
            }).then(function(result){
                self.$('.btn-centre').append(QWeb.render('mail.custom_like_button', {
                    // displayCounter: !!this.fields.thread,
                    approved_count: result.approved_count || 0,
                    rejected_count: result.rejected_count || 0,
                    total_count: result.total_count || 0,
                }));
            });


        return this._super.apply(this, arguments);
    },

    // Play Button Method
    _onPlayButton: function (ev) {

        ev.preventDefault();
        var self = this;
        var PlayBtn = $(ev.currentTarget);
        var context = self.context
        self._rpc({
            model: 'mail.approvers',
            method: 'action_mail_approval_start',
            args: [[]],
            context: context,
        }).then(function (result){
            self.trigger_up('reload');
        });
    },

    // Pause Button Method
    _onPauseButton: function (ev) {
        ev.preventDefault();
        var self = this;
        var PauseBtn = $(ev.currentTarget);
        var context = self.context
        self._rpc({
            model: 'mail.approvers',
            method: 'action_mail_approval_pause',
            args: [[]],
            context: context,
        }).then(function (result){
            self.trigger_up('reload');
        });
    },


    // Stop Button Method
    _onStopButton: function (ev) {
        ev.preventDefault();
        var self = this;
        var StopBtn = $(ev.currentTarget);
        var context = self.context
        self._rpc({
            model: 'mail.approvers',
            method: 'action_mail_approval_stop',
            args: [[]],
            context: context,
        }).then(function (result){
            self.trigger_up('reload');
        });
    },

    // Approvers Button Method
    _onApproversButton: function (ev) {
        ev.preventDefault();
        var self = this;
        var ApproverBtn = $(ev.currentTarget);
        var res_id = self.context.default_res_id;
        var res_model = self.context.default_model;
        self.do_action({
            type: 'ir.actions.act_window',
            res_model: 'mail.approvers',
            res_id: self.id,
            domain:[['res_id','=',res_id],['res_model','=',res_model]],
            context: {
                'res_id': res_id,
                'res_model':res_model,
                'default_res_id': res_id,
                'default_res_model':res_model,
            },
            views: [ [false, 'list']],
            target:'new',
        });

    },

})

});