{
    'name': "Approval Mixin",
    'description': "Module for approval in msg threads",
    'author': 'Solevo,Ehtisham Faisal',
    'category': 'mail',
    'version': '12.0.2',
    'application': True,
    'depends': ['base', 'mail','sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/mail_mix_view.xml',
        'views/mail_approvers_view.xml',
        'report/report.xml',
        'report/module_report.xml',
    ],
    'qweb': [
        "static/src/xml/mail_activity_template.xml",
    ],
}