#-*- coding:utf-8 -*-
########################################################################################
########################################################################################
##                                                                                    ##
##    OpenERP, Open Source Management Solution                                        ##
##    Copyright (C) 2011 OpenERP SA (<http://openerp.com>). All Rights Reserved       ##
##                                                                                    ##
##    This program is free software: you can redistribute it and/or modify            ##
##    it under the terms of the GNU Affero General Public License as published by     ##
##    the Free Software Foundation, either version 3 of the License, or               ##
##    (at your option) any later version.                                             ##
##                                                                                    ##
##    This program is distributed in the hope that it will be useful,                 ##
##    but WITHOUT ANY WARRANTY; without even the implied warranty of                  ##
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   ##
##    GNU Affero General Public License for more details.                             ##
##                                                                                    ##
##    You should have received a copy of the GNU Affero General Public License        ##
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.           ##
##                                                                                    ##
########################################################################################
########################################################################################

from odoo import api, models, fields
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.exceptions import Warning

class HrSolevoTimesheetReport(models.AbstractModel):
	_name = 'report.hr_solevo_timesheet_report.hr_solevo_timesheet_id'

	@api.model
	def _get_report_values(self, docids, data=None):
		self.model = self.env.context.get('active_model')
		record_wizard = self.env[self.model].browse(self.env.context.get('active_id'))

		form = record_wizard.form
		to = record_wizard.to
		employee_id = record_wizard.employee_id
		project_id = record_wizard.project_id
		head = "Timesheet Enteries"

		employee_ids = []
		if employee_id:
			for emp in employee_id:
				employee_ids.append(emp)
		else:
			employee_ids = self.env['hr.employee'].search([])

		project_ids = []
		if project_id:
			for pro in project_id:
				project_ids.append(pro.id)
		else:
			project_ids = self.env['project.project'].search([]).ids

		main_data = []
		for x in employee_ids:
			dates_data = []
			date_wise = []
			project_name = []
			dates_recs = self.env['account.analytic.line'].search([('date','>=',form),('date','<=',to),('project_id.id','in',project_ids),('employee_id.id','=',x.id)])
			for d in dates_recs:
				if d.date not in dates_data:
					dates_data.append(d.date)
				if d.project_id not in project_name:
					project_name.append(d.project_id)

			for y in dates_data:
				task_recs = self.env['account.analytic.line'].search([('date','>=',form),('date','<=',to),('project_id.id','in',project_ids),('employee_id.id','=',x.id),('date','=',y)])

				date_wise.append({
						'task_recs':task_recs,
						'date':y,
					})

			if len(date_wise) > 0:

				main_data.append({
					'date_wise':date_wise,
					'project_name':project_name,
					'project_size':len(project_name),
					'empolyee':x.name,
					'id':x.id,
				})

		def get_time(attr,attr1):
			task_recs = self.env['account.analytic.line'].search([('date','>=',form),('date','<=',to),('id','=',attr1),('project_id.id','=',attr)])
			if task_recs:
				return task_recs.unit_amount
			else:
				return 0

		def get_total_time(attr,attr1):
			tot = 0.0
			task_recs = self.env['account.analytic.line'].search([('date','>=',form),('date','<=',to),('employee_id.id','=',attr1),('project_id.id','=',attr)])
			for t in task_recs:
				tot = tot + t.unit_amount

			return tot


		return {
			'doc_ids': docids,
			'doc_model':'account.analytic.line',
			'form': form,
			'to': to,
			'head': head,
			'main_data': main_data,
			'get_time': get_time,
			'get_total_time': get_total_time,
		}
