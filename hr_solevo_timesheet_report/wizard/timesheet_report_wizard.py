# #-*- coding:utf-8 -*-

import os
import xlsxwriter
from datetime import date
from datetime import date, timedelta
import datetime
import time
from odoo import api, models, fields
from odoo.exceptions import Warning,ValidationError
from odoo.tools import config
import base64
import string
import sys

class HrSolevoTimesheetWizard(models.TransientModel):
	_name = "hr.solevo.timesheet.wizard"
	
	# Fields declaration
	form = fields.Date(string="From")
	to = fields.Date(string="To")
	employee_id = fields.Many2many('hr.employee',string="Employees")
	project_id = fields.Many2many('project.project',string="Projects")

	# Gennerate report method called via button
	@api.multi
	def generate_report(self):
		data = {}
		data['form'] = self.read(['form','to','employee_id','project_id'])[0]
		return self._print_report(data)
	# return report action 
	def _print_report(self, data):
		data['form'].update(self.read(['form','to','employee_id','project_id'])[0])
		return self.env.ref('hr_solevo_timesheet_report.report_for_hr_solevo_timesheet').report_action(self, data=data)
	