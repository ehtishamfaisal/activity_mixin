# -*- coding: utf-8 -*-
{
    'name': "solevo_mail_approvers",

    'summary': """
        Create a workflow for record approval using the mail.thread. """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Solevo",
    'website': "https://solevo.ca",

    'category': 'Tool',
    'version': '12.0.2',

    # any module necessary for this one to work correctly
    'depends': ['base', 'web', 'project', 'web_widget_digitized_signature', 'compoze_no_auto_subscribe'],
    'qweb': ['static/src/xml/template.xml'],

    # always loaded
    'data': [
        #'security/groups.xml',
        #'security/ir.model.access.csv',
        #'views/approvers_view.xml',
        'data/mail_activity_type.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],

}