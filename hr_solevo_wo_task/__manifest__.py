{
    'name': "hr_solevo_wo_task",
    'description': "Hr Timesheet Report without task",
    'author': 'Ehtisham Faisal',
    'category': 'account',
    'version': '12.0.2',
    'application': True,
    'depends': ['base', 'account','project'],
    'data': [
        'security/ir.model.access.csv',
        'report/report.xml',
        'report/module_report.xml',
        'wizard/timesheet_report_wizard_view.xml',
    ],
}